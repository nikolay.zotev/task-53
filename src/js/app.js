window.addEventListener("DOMContentLoaded", () => {
  const productElement = document.querySelector(".product");
  const priceSpan = document.querySelector(".price").innerHTML;

  productElement.setAttribute("data-price", priceSpan);
});
